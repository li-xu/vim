=======================
common unix config file
=======================

---------------------
Installing custom zsh
---------------------
Create symbolic link for alias::

	$ cd ~
	# Create symbolic link for alias
	$ ln -s ~/vim/.keyshortcut .keyshortcut
	$ echo "source ~/.keyshortcut" >> .zshrc
	# Remove prompt
	$ echo "PROMPT=''" >> .zshrc
	# Use vi editing
	$ echo "bindkey -v" >> .zshrc

----------------------------
Installing custom Bash shell
----------------------------
Create symbolic links (assuming vim git directory at ~/vim)::

        $ cd ~
        $ ln -s ~/vim/.bashrc_custom .bashrc_custom

Create .bash_profile in home directory and add following lines::

        if [ -f ~/.bashrc_custom ]; then
        . ~/.bashrc_custom
        fi

-------------------------------
Installing custom Vim behaviour
-------------------------------
::

        $ cd ~
        $ ln -s ~/vim/.vimrc .vimrc
        $ ln -s ~/vim .vim

-----------------------------
Installing vi keymap in shell
-----------------------------
::

        $ cd ~
        $ ln -s ~/vim/.inputrc .inputrc

---------------
Updating plugin
---------------

plugin/linuxsty.vim
===================
use Linux kernel coding guideline for eding C lang code.
https://github.com/vivien/vim-linux-coding-style

plugin/cscope_maps.vim
======================
Used by cscope.
http://cscope.sourceforge.net/cscope_maps.vim

vim-go
======
Go Language programming tool box
$ git clone https://github.com/fatih/vim-go.git ~/.vim/pack/plugins/start/vim-go

To install help file, in Vim
:helptags ALL

Ensure following line exists in .vimrc
filetype plugin indent on

Tagbar
======
$ git clone https://github.com/majutsushi/tagbar.git ~/.vim/pack/plugins/start/tagbar

Ensure in Vim
:filetype on

Install Exuberant Ctags
ctags.sourceforge.net

gotag
=====
Tagbar for Go Langauge
$ go get -u github.com/jstemmer/gotags

----------------
Change Time Zone
----------------
$ sudo mv /etc/localtime /etc/localtime.default
$ sudo ln -s /usr/share/zoneinfo/America/Chicago /etc/localtime

------------------------
Customize mutt behaviour
------------------------
Create ~/.mutt/muttrc to custormize mutt behaviour.
muttrc sample exists inside the repository.

On Ubuntu, Mutt can be installed using following commands::

        $ sudo apt-get install mutt
        $ sudo apt-get install mutt-patched
