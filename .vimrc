set noexpandtab
set shiftwidth=8
set tabstop=8

"enable mouse cursor selection
set mouse=a

"shortcut to switch to next buffer
nmap ' :bn<Enter>

"switch to next window
nmap tt <c-w><c-w>
"switch to left window
nmap th <c-w>h
"switch to right window
nmap tl <c-w>l

nmap <Space> <PageDown>

"status line
"show file full path, line number, and column number
set statusline=%F\ %l/\ %c
"always show stat line
set laststatus=2

"set text wraparound position
set textwidth=80

set autoindent

syntax off

"enable incremental search
set incsearch

"disable matching brackets
set noshowmatch
let g:loaded_matchparen=1

"delete buffer without closing all window, bd command close all window
command Bc bp<bar>sp<bar>bn<bar>bd
